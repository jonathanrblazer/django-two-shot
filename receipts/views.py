from django.shortcuts import render
from receipts.models import Receipt, ExpenseCategory
from receipts.forms import ReceiptForm, CategoryForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts_list": receipts}
    return render(request, "receipts/list.html", context)


def redirect_to_receipt_list(request):
    return redirect("home")


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.purchaser = request.user
            recipe.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    # Put the form in the context
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categorys = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categorys_list": categorys}
    return render(request, "receipts/categories/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

    # Put the form in the context
    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create.html", context)
